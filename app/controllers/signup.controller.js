const db = require("../models");
const users = db.users
module.exports={
	signup: signup
}
async function signup(req,res){
	const name = req.body.name
	const email = req.body.email
	const password = req.body.password
	const gender = req.body.gender
	const mobile = req.body.mobile

	if (!req.body.name) {
	    res.status(400).send({ status:0, message: "Name can not be empty!" });
	    return;
	}
	if (!req.body.email) {
	    res.status(400).send({ status:0, message: "Email can not be empty!" });
	    return;
	}
	if (!req.body.password) {
	    res.status(400).send({ status:0, message: "Password can not be empty!" });
	    return;
	}
	if (!req.body.gender) {
	    res.status(400).send({ status:0, message: "Gender can not be empty!" });
	    return;
	}
	if (!req.body.mobile) {
	    res.status(400).send({ status:0, message: "Mobile can not be empty!" });
	    return;
	}
	console.log(db)
	const user = new users ({
	    name,
	    email,
	    password,
	    gender,
	    mobile
	});
	users.find({ email})
    .then(data => {
    	if(data.length == 0) {
			user
		    .save(user)
		    .then(data => {
		      res.send({ status:1,data});
		    })
		    .catch(err => {
		      res.status(500).send({
		        message:
		          err.message || "Some error occurred while creating the Tutorial."
		      });
		    });
		} else {
	   		 res.status(400).send({ status:0, message: "Email ID Already Exist" });
		}
	})
}