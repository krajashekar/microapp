var signupController=require('./signup.controller');
var loginController=require('./login.controller');
module.exports={
	userSignup: signupController.signup,
	userLogin: loginController.login
}
